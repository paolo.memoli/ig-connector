const got = require('got')
const {
    Subscription,
    LightstreamerClient
} = require('lightstreamer-client-node')

class IG {
    constructor(config) {
        this.api_key = config.api_key
        this.id = config.id
        this.password = config.password
        this.url = config.url
        this.headers = {
            "X-IG-API-KEY": this.api_key,
            "Content-Type": "application/json",
        }
        this.user = {}

    }
    options(version = 3, request_headers = this.headers) {
        return {
            prefixUrl: `${this.url}/gateway/deal`,
            responseType: 'json',
            headers: {
                ...request_headers,
                "Version": version,
            },
        }
    }
    async login() {
        this.user = await got.post('session', {
            ...this.options(),
            json: {
                "identifier": this.id,
                "password": this.password,
            },
            responseType: 'json'
        }).json()

        this.headers["Authorization"] = `Bearer ${this.user.oauthToken.access_token}`
        this.headers["IG-ACCOUNT-ID"] = this.user.accountId
        let session = await got("session?fetchSessionTokens=true", this.options(1))
        this.user.lightToken = {
            cst: session.headers["cst"],
            xst: session.headers["x-security-token"],
        }
    }

    async positions(epic) {
        return (await got('/positions', options).json()).positions.find(position_obj => position_obj.market.epic == epic)
    }

    async stream_data(epics, scale, update) {
        if (!this.user.lightToken) await this.login()
        try {
            let lsClient = new LightstreamerClient(this.user.lightstreamerEndpoint);
            lsClient.connectionDetails.setUser(this.user.accountId);
            lsClient.connectionDetails.setPassword('CST-' + this.user.lightToken.cst + '|XST-' + this.user.lightToken.xst);
            lsClient.addListener({
                onStatusChange: status => console.log('Lightstreamer connection status: ' + status),
                onServerError: (errorCode, errorMessage) => console.log('Lightstreamer error: ' + errorCode + ' message: ' + errorMessage)
            });
            lsClient.connect();
            let subscription = new Subscription('MERGE', epics.map(epic => `CHART:${epic}:${scale}`), ['BID_HIGH', 'BID_LOW', 'BID_CLOSE', 'UTM']);
            subscription.setRequestedMaxFrequency(0.5);
            subscription.addListener({
                onSubscriptionError: (code, message) => console.log('Subscription failure: ' + code + ' message: ' + message),
                onItemUpdate: updateInfo => {
                    console.log(updateInfo)
                    update("things")
                }
            });
            lsClient.subscribe(subscription);
        } catch (error) {
            console.log(error)
        }
    }
    async open(point, direction = "BUY", size = 0.5, stop = 5, limit = 5) {
        let deal = await got.post('/workingorders/otc', {
            ...this.options(2),
            json: {
                'currencyCode': 'GBP',
                'direction': direction,
                'epic': point.epic,
                'expiry': 'DFB',
                'size': size,
                'forceOpen': true,
                'orderType': 'MARKET',
                'limitDistance': limit,
                'stopDistance': stop,
                'guaranteedStop': false,
                'timeInForce': 'FILL_OR_KILL',
            }
        }).json()
        console.log(`opened ${direction} position on ${point.epic}`)
        return deal
    }
    async close(point) {

        let position_obj = await positions(point.epic)

        let deal = await got.delete('/workingorders/otc', {
            ...this.options(1),
            json: {
                dealId: position_obj.position.dealId
            }
        }).json()

        position_obj.profit = {
            direction: position_obj.position.direction,
            from: position_obj.position.openLevel,
            to: position_obj.position.direction == "BUY" ? position_obj.market.bid : position_obj.market.offer,
            size: position_obj.position.dealSize,
        }

        position_obj.profit.total = (position_obj.profit.size * position_obj.profit.direction == "BUY" ? position_obj.profit.from - position_obj.profit.to : position_obj.profit.to - position_obj.profit.from).toFixed(2)
        console.log(`closed deal on: ${point.epic} for profit: £${position_obj.profit.total}`)
        return position_obj
    }

    async historical(epic, scale = "HOUR", numPoints, save, convertTime) {
        if (!this.user.oauthToken) await this.login()
        try {

            let response = await got(`prices/${epic}?resolution=${scale}&max=${numPoints}`, this.options()).json()
            if (!response.prices) throw response

            let points = response.prices.map(raw_point => ({
                epic: epic,
                timestamp: convertTime(raw_point.snapshotTime),
                close: raw_point.closePrice.bid,
                high: raw_point.highPrice.bid,
                low: raw_point.lowPrice.bid,
            }))

            for (const point of points) {
                if (save) await save(point)
            }

            return points
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = IG